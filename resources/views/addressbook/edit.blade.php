@extends('layouts.app')

@section('content')

    <h1>Edit Address Book</h1>
    <hr/>

    {!! Form::model($addressbook, [
        'method' => 'PATCH',
        'url' => ['addressbook', $addressbook->id],
        'class' => 'form-horizontal'
    ]) !!}

                <div class="form-group {{ $errors->has('address_book_title') ? 'has-error' : ''}}">
                {!! Form::label('address_book_title', 'Address Book Title: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('address_book_title', array('Home' => 'Home', 'Work' => 'Work'), ['class' => 'form-control']) !!}
                    {!! $errors->first('address_book_title', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('contact_person_name') ? 'has-error' : ''}}">
                {!! Form::label('contact_person_name', 'Contact Person Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('contact_person_name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('contact_person_name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('contact_person_number') ? 'has-error' : ''}}">
                {!! Form::label('contact_person_number', 'Contact Person Number: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('contact_person_number', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('contact_person_number', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('address_line_1') ? 'has-error' : ''}}">
                {!! Form::label('address_line_1', 'Address Line 1: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('address_line_1', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('address_line_1', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('address_line_2') ? 'has-error' : ''}}">
                {!! Form::label('address_line_2', 'Address Line 2: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('address_line_2', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('address_line_2', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('address_line_3') ? 'has-error' : ''}}">
                {!! Form::label('address_line_3', 'Address Line 3: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('address_line_3', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('address_line_3', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('pincode') ? 'has-error' : ''}}">
                {!! Form::label('pincode', 'Pincode: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('pincode', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('pincode', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('city') ? 'has-error' : ''}}">
                {!! Form::label('city', 'City: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('city', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('state') ? 'has-error' : ''}}">
                {!! Form::label('state', 'State: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('state', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('country') ? 'has-error' : ''}}">
                {!! Form::label('country', 'Country: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('country', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('default_from') ? 'has-error' : ''}}">
                {!! Form::label('default_from', 'Default From: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('default_from', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('default_from', '0', true) !!} No</label>
            </div>
                    {!! $errors->first('default_from', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('default_to') ? 'has-error' : ''}}">
                {!! Form::label('default_to', 'Default To: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('default_to', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('default_to', '0', true) !!} No</label>
            </div>
                    {!! $errors->first('default_to', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
           <div class="form-group">
                <input type="hidden" name="user" value="{{ $addressbook->user }}">
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection