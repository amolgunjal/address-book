@extends('layouts.app')

@section('content')

    <h1>Address Book <a href="{{ url('addressbook/create') }}" class="btn btn-primary pull-right btn-sm">Add New Address</a></h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>S.No</th><th>Address Book Title</th><th>Contact Person Name</th><th>Contact Person Number</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($addressbook as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $x }}</td>
                    <td><a href="{{ url('addressbook', $item->id) }}">{{ $item->address_book_title }}</a></td><td>{{ $item->contact_person_name }}</td><td>{{ $item->contact_person_number }}</td>
                    <td>
                        <a href="{{ url('addressbook/' . $item->id . '/edit') }}">
                            <button type="submit" class="btn btn-primary btn-xs">Update</button>
                        </a> /
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['addressbook', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination"> {!! $addressbook->render() !!} </div>
    </div>

@endsection
