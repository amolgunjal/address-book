@extends('layouts.app')

@section('content')

    <div class="row">
        
            <div class="panel panel-default">
                <div class="panel-heading">Api Details</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1"><b>Api url</b>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            {{ 'http://address.book:8000/api/users/addressbook/'. $user_id }}
                        </div>
                    </div>
                </div>
                </div>
            </div>

    </div>

@endsection
