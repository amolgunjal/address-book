<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\BaseController;
use App\Http\Requests;
use Illuminate\Http\Request;
//use App\Http\Transformers\UserTransformer;
use App\Http\Transformers\AddressBookTransformer;
use App\Addressbook;
use Auth;

class UserApiController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAddressBook($user_id)
    {
        //$user = User::findOrFail($user_id);
        //return $this->response->array($user->toArray());
        //return $this->response->item($user, new UserTransformer);
        
         $addressbooks_of_user = Addressbook::where('user', '=', $user_id)->get();
         return $this->response->collection($addressbooks_of_user, new AddressBookTransformer);
    }
}