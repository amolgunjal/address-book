<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::auth();
Route::get('/home', 'HomeController@index');
Route::get('/profile', 'UserController@profile');
Route::group(['middleware' => ['web', 'auth']], function () {
	Route::resource('addressbook', 'AddressbookController');
});
Route::get('/apidetails', 'UserController@apiDetails');



// Api Routes
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function($api){
    $api->get('users/addressbook/{user_id}', 'App\Http\Controllers\UserApiController@getAddressBook');
});