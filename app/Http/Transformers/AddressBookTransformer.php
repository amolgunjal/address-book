<?php 

namespace App\Http\Transformers;

use App\Addressbook;
use League\Fractal\TransformerAbstract;

class AddressBookTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param  \App\User  $user
     * @return array
     */
    public function transform(Addressbook $addressbook)
    {
        return [
            'id'                        => $addressbook->id,
            'address_book_title'        => $addressbook->address_book_title,
            'contact_person_name'       => $addressbook->contact_person_name,
            'contact_person_number'     => $addressbook->contact_person_number,
            'address_line_1'            => $addressbook->address_line_1,
            'address_line_2'            => $addressbook->address_line_2,
            'address_line_3'            => $addressbook->address_line_3,
            'pincode'                   => $addressbook->pincode,
            'city'                      => $addressbook->city,
            'state'                     => $addressbook->state,
            'country'                   => $addressbook->country,
            'default_from'              => $addressbook->default_from,
            'default_to'                => $addressbook->default_to,
            'user'                      => $addressbook->user,
            'created_at'                => $addressbook->created_at->toDateTimeString(),
            'updated_at'                => $addressbook->updated_at->toDateTimeString()
        ];
    }
}