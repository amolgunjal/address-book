<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addressbook extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'addressbooks';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['address_book_title', 'contact_person_name', 'contact_person_number', 'address_line_1', 'address_line_2', 'address_line_3', 'pincode', 'city', 'state', 'country', 'default_from', 'default_to', 'user'];

    public function addressDefaultFrom($addressbook)
    {

    }

    public function addressDefaultTo($addressbook)
    {

    }
}
