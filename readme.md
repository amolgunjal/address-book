# Address Book

**Features:**

- User registration.
- login.
- Manage logged in user profile.
- Manage Address Book (Add, Edit. Delete, Show).
- API - Get all address of user.

**Configuration:**

* Configure database & Api in .env file.